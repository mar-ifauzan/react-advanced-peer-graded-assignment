import React from "react";
import FullScreenSection from "./FullScreenSection";
import { Box, Heading } from "@chakra-ui/react";
import Card from "./Card";

const projects = [
  {
    title: "Bucket List Destinations",
    description:
      "A compilation of dream travel destinations, from the iconic wonders of Machu Picchu to the tranquil beaches of the Maldives.",
    getImageSrc: () => require("../images/photo1.jpg"),
  },
  {
    title: "Healthy Habits",
    description:
      "A checklist of daily routines to boost well-being, including exercise, mindfulness, and balanced nutrition.",
    getImageSrc: () => require("../images/photo2.jpg"),
  },
  {
    title: "Classic Novels",
    description:
      'A selection of timeless literary masterpieces, such as "To Kill a Mockingbird" and "Pride and Prejudice.',
    getImageSrc: () => require("../images/photo3.jpg"),
  },
  {
    title: "Home Improvement Projects",
    description:
      "A set of DIY tasks to enhance your living space, ranging from repainting walls to building custom furniture.",
    getImageSrc: () => require("../images/photo4.jpg"),
  },
];

const ProjectsSection = () => {
  return (
    <FullScreenSection
      backgroundColor="#14532d"
      isDarkBackground
      p={8}
      alignItems="flex-start"
      spacing={8}
    >
      <Heading as="h1" id="projects-section">
        Featured Projects
      </Heading>
      <Box
        display="grid"
        gridTemplateColumns="repeat(2,minmax(0,1fr))"
        gridGap={8}
      >
        {projects.map((project) => (
          <Card
            key={project.title}
            title={project.title}
            description={project.description}
            imageSrc={project.getImageSrc()}
          />
        ))}
      </Box>
    </FullScreenSection>
  );
};

export default ProjectsSection;
