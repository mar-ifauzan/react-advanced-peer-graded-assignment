import {
  Box,
  Heading,
  HStack,
  Image,
  Stack,
  Text,
  VStack,
} from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import React from "react";

const Card = ({ title, description, imageSrc }) => {
  return (
    <Box borderRadius={12} backgroundColor={"white"} height={600}>
      <Image borderTopRadius={12} width={"fit-content"} src={imageSrc} />
      <Heading as="h4" size="md" px={5} pt={5} color={"black"}>
        {title}
      </Heading>
      <Text fontSize="sm" textColor={"gray.400"} px={5} pt={5} color={"black"}>
        {description}
      </Text>

      <a
        href="_blank"
        target="_blank"
        rel="noopener noreferrer"
        style={{ color: "black", padding: 20 }}
      >
        See More <FontAwesomeIcon icon={faArrowRight} size="1x" />
      </a>
    </Box>
  );
};

export default Card;
